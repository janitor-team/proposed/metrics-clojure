#!/usr/bin/make -f

include /usr/share/javahelper/java-vars.mk

MDWN_DOCS = $(patsubst %.md,%.html,$(wildcard $(CURDIR)/*.md))

%:
	dh $@ --with javahelper --with jh_maven_repo_helper

override_jh_build: $(MDWN_DOCS)
	jar cf metrics-clojure.jar -C metrics-clojure-core/src .
	jar cf metrics-clojure-jvm.jar -C metrics-clojure-jvm/src .
	jar cf metrics-clojure-ganglia.jar -C metrics-clojure-ganglia/src .
	jar cf metrics-clojure-health.jar -C metrics-clojure-health/src .
	jar cf metrics-clojure-ring.jar -C metrics-clojure-ring/src .
	mkdir -p $(CURDIR)/doc/html && mv $(CURDIR)/*.html $(CURDIR)/doc/html

override_jh_clean:
	jh_clean
	rm -f $(CURDIR)/metrics-clojure*.jar
	rm -rf $(CURDIR)/doc/html

override_dh_installdocs:
	dh_installdocs -XChangeLog

%.html:%.md
	cat debian/header.html > $@
	sed -i'' -e 's#@TITLE@#$(shell head -n 1 $< | sed 's/^#*\s*//')#g' $@
	markdown $< >> $@
	cat debian/footer.html >> $@

override_dh_auto_test:
	dh_auto_test
	(cd metrics-clojure-core/test && find . -name '*.clj' | \
		xargs -n 1 --verbose clojure -cp $(CURDIR)/metrics-clojure.jar:/usr/share/java/metrics-core.jar:/usr/share/java/slf4j-api.jar:.)
	(cd metrics-clojure-ring/test && find . -name '*.clj' | \
		xargs -n 1 --verbose clojure -cp $(CURDIR)/metrics-clojure-ring.jar:$(CURDIR)/metrics-clojure.jar:/usr/share/java/metrics-core.jar:/usr/share/java/cheshire.jar:/usr/share/java/ring-core.jar:.)
